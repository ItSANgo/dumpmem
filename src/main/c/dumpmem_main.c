/**
 * @file dumpmem_main.c
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief dump memory - main.
 * @version 0.1.0
 * @date 2019-12-31
 * 
 * @copyright Copyright (c) 2019 Mitsutoshi Nakano
 * 
 */

#include "dumpmem.h"

#include <err.h>
#include <sysexits.h>
#include <getopt.h>
#include <unistd.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

/**
 * @brief main.
 * 
 * @param argc arguments.
 * @param argv arguments.
 * @return int exit status.
 */
int
main(int argc, char *argv[])
{
    char *outputfile = NULL;
    for (;;) {
        static struct option options[] = {
            { "help", 0, NULL, 'h' },
            { "output", 1, NULL, 'o' },
            { NULL, 0, NULL, 0 },
        };
        int option_index = 0;
        int c = getopt_long(argc, argv, "ho:", options, &option_index);
        if (c == -1) {
            break;
        }
        switch (c) {
        case 'h':
            help(EX_OK, stdout, argv[0]);
            /*NOTREACHED*/
            break;
        case 'o':
            if (outputfile) {
                errx(EX_USAGE, "output couldn't duplicate %s, %s\n", outputfile, optarg);
                /*NOTREACHED*/
            }
            outputfile = optarg;
            break;
        case '?':
            help(EX_USAGE, stderr, argv[0]);
            /*NOTREACHED*/
            break;
        default:
            assert(0);
            break;
        }
    }
    if (optind != argc - 2) {
        help(EX_USAGE, stderr, argv[0]);
        /*NOTREACHED*/
    }
    unsigned long long address = strtoull(argv[optind], (char **)NULL, 0);
    unsigned long long length = strtoull(argv[optind + 1], (char **)NULL, 0);
    FILE *outfp = NULL;
    if (outputfile == NULL) {
        outfp = stdout;
    } else {
        outfp = fopen(outputfile, "w");
        if (outfp == NULL) {
            err(EX_CANTCREAT , "fopen(%s)", outputfile);
            /*NOTREACHED*/
        }
    }
    dumpmem((const unsigned char *)address, length, outfp);
    exit(EX_OK);
}