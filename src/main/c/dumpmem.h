#ifndef DUMPMEM_H
#define DUMPMEM_H
/**
 * @file dumpmem.h
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief dump memory - main.
 * @version 0.1.0
 * @date 2019-12-31
 * 
 * @copyright Copyright (c) 2019 Mitsutoshi Nakano
 * 
 */

#include <stdio.h>

extern void help(int status, FILE *fp, const char *command);
extern int dumpmem(const unsigned char *start, unsigned long long length, FILE *outfp);

#endif /* DUMPMEM_H.  */
