/**
 * @file dumpmem.c
 * @author Mitsutoshi Nakano <ItSANgo@gmail.com>
 * @brief dump memory
 * @version 0.1.0
 * @date 2019-12-31
 * 
 * @copyright Copyright (c) 2019 Mitsutoshi Nakano
 * 
 */

#include "dumpmem.h"

#include <stdlib.h>
#include <errno.h>
#include <stdio.h>

/**
 * @brief help and bye.
 * 
 * @param status exit status.
 * @param fp outout file.
 * @param command command name.
 */
void
help(int status, FILE *fp, const char *command)
{
    fprintf(fp, "usage: %s [--help] [-o output_file] address length\n", command);
    exit(status);
}

/**
 * @brief dump memory.
 * 
 * @param start address.
 * @param length lenghth.
 * @param outfp output file.
 * @return int erro number if error occurs.
 */
int
dumpmem(const unsigned char *start, unsigned long long length, FILE *outfp)
{
    int error_number = 0;
    unsigned long long current;
    for (current = 0; current < length; ++current) {
        if (fputc(*(start + current), outfp) < 0) {
            error_number = errno;
        }
    }
    return error_number;
}
